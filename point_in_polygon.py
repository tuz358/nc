#!/usr/bin/python3
#-*- coding: utf-8 -*-
# 
# Crossing Number Algorithm

import matplotlib.pyplot as plt
import numpy as np

area = {'x': 100, 'y': 100} # 解析領域(100x100)
vertices = [(2.0, 3.0), (3.0, 4.0), (4.0, 4.0), (5.0, 2.0), (3.0, 1.0)]
points = np.random.rand(1000, 2) * 7.0 + 0.5

def is_cross(p1, p2, p3, p4):
    '''
    p1とp2が線分, p3とp4が線分
    '''
    tc1 = (p1[0]-p2[0])*(p3[1]-p1[1]) + (p1[1]-p2[1])*(p1[0]-p3[0])
    td1 = (p1[0]-p2[0])*(p4[1]-p1[1]) + (p1[1]-p2[1])*(p1[0]-p4[0])
    tc2 = (p3[0]-p4[0])*(p1[1]-p3[1]) + (p3[1]-p4[1])*(p3[0]-p1[0])
    td2 = (p3[0]-p4[0])*(p2[1]-p3[1]) + (p3[1]-p4[1])*(p3[0]-p2[0])
    return tc1*td1<0 and tc2*td2<0

def point_in_polygon(p):
    inside = False
    for i in range(len(vertices)):
        j = (i+1)%len(vertices)
        if is_cross(p, [area['x'], p[1]], vertices[i], vertices[j]):
            inside = not inside
    return inside

fig = plt.figure()
ax = fig.add_subplot(111)
poly = plt.Polygon(xy=vertices, color='#4f4f4f33')
ax.add_patch(poly)

for p in points:
    ret = point_in_polygon(p)
    color = '#ff0000' if ret else '#000000'
    plt.plot(p[0], p[1], marker='.', color=color)

plt.xlim([0, 8])
plt.ylim([0, 8])
plt.show()

