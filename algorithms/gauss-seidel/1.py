#!/usr/bin/python3
#-*- coding: utf-8 -*-

import numpy as np


def gauss_seidel(A, b):
    # A = L + U
    L = np.tril(A)
    L_inv = np.linalg.inv(L)
    U = A - L
    x = np.ones_like(b)
    x_new = np.zeros_like(b)

    i = 0
    eps = 1e10
    EPS = 1e-9
    while EPS < eps and i < 1e10:
        x_new = np.dot(L_inv, b - np.dot(U, x))
        eps = np.linalg.norm(x_new - x, np.inf)
        x = x_new
        i += 1

    return x_new

def main():
    A = np.array([[7, -1, 0, 1],
        [-1, 9, -2, 2],
        [0, -2, 8, -3],
        [1, 2, -3, 10],
        ])
    b = np.array([-5, 15, -10, 20])

    x = gauss_seidel(A, b)
    print(x)
    print(np.linalg.inv(A).dot(b))

if __name__=='__main__':
    main()
