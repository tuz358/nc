#!/usr/bin/python3
#-*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np


def f(x, y):
    return np.cos(x)

def main():
    y = np.array([])
    h = 0.1
    y0 = 0

    t = np.arange(0, 50, h)
    for i in t:
        k1 = h*f(i, y0)
        k2 = h*f(i+h/2, y0+k1/2)
        k3 = h*f(i+h/2, y0+k2/2)
        k4 = h*f(i+h, y0+k3)

        y0 += (k1 + 2*k2 + 2*k3 + k4)/6
        y = np.append(y, y0)

    plt.plot(t, y)
    plt.plot(t, np.sin(t)) # 解析解
    plt.show()

if __name__=='__main__':
    main()

