#!/usr/bin/python3
#-*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np


# 1階連立常微分方程式
# y'(x, y, z) = y - z
# z'(x, y, z) = 2y + 4z
def f(x, y, z):
    return y - z

def g(x, y, z):
    return 2*y + 4*z

def main():
    a = 0
    b = 10
    n = 100
    h = (b - a)/n
    x = np.zeros(n)
    y = np.zeros(n)
    z = np.zeros(n)
    x[0] = a
    y[0] = 1
    z[0] = 3

    for i in range(n-1):
        k1_y = h*f(x[i], y[i], z[i])
        k1_z = h*g(x[i], y[i], z[i])
        k2_y = h*f(x[i]+h/2, y[i]+k1_y/2, z[i]+k1_z/2)
        k2_z = h*g(x[i]+h/2, y[i]+k1_y/2, z[i]+k1_z/2)
        k3_y = h*f(x[i]+h/2, y[i]+k2_y/2, z[i]+k2_z/2)
        k3_z = h*g(x[i]+h/2, y[i]+k2_y/2, z[i]+k2_z/2)
        k4_y = h*f(x[i]+h, y[i]+k3_y, z[i]+k3_z)
        k4_z = h*g(x[i]+h, y[i]+k3_y, z[i]+k3_z)

        x[i+1] = x[i] + h
        y[i+1] = y[i] + (k1_y + 2*k2_y + 2*k3_y + k4_y)/6
        z[i+1] = z[i] + (k1_z + 2*k2_z + 2*k3_z + k4_z)/6

    x2 = np.linspace(a, b, num=1000)
    y2 = np.zeros(1000)
    z2 = np.zeros(1000)
    for i in range(1000):
        y2[i] = 5*np.exp(2*x2[i]) - 4*np.exp(3*x2[i])
        z2[i] = -5*np.exp(2*x2[i]) + 8*np.exp(3*x2[i])

    plt.plot(x, y, label='Numerical x-y')
    plt.plot(x, z, label='Numerical x-z')
    plt.plot(x2, y2, label='Analytic x-y')
    plt.plot(x2, z2, label='Analytic x-z')
    plt.legend()
    plt.show()

if __name__=='__main__':
    main()

