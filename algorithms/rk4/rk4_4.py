#!/usr/bin/python3
#-*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np

# 2階常微分方程式
# y"(t, y) = -y (単振動)
#
# 式変形により連立常微分方程式になる
# y'1(t, y1, y2) = y2
# y'2(t, y1, y2) = -y1
def f(t, y1, y2):
    return y2

def g(t, y1, y2):
    return -y1

def main():
    a = 0
    b = 30
    n = 70
    h = (b - a)/n
    t = np.zeros(n)
    y = np.zeros(n)
    y1 = np.zeros(n)
    y2 = np.zeros(n)
    t[0] = a
    y[0] = 1
    y1[0] = 1
    y2[0] = 0

    for i in range(n-1):
        k1_1 = h*f(t[i], y1[i], y2[i])
        k1_2 = h*g(t[i], y1[i], y2[i])
        k2_1 = h*f(t[i]+h/2, y1[i]+k1_1/2, y2[i]+k1_2/2)
        k2_2 = h*g(t[i]+h/2, y1[i]+k1_1/2, y2[i]+k1_2/2)
        k3_1 = h*f(t[i]+h/2, y1[i]+k2_1/2, y2[i]+k2_2/2)
        k3_2 = h*g(t[i]+h/2, y1[i]+k2_1/2, y2[i]+k2_2/2)
        k4_1 = h*f(t[i]+h, y1[i]+k3_1, y2[i]+k3_2)
        k4_2 = h*g(t[i]+h, y1[i]+k3_1, y2[i]+k3_2)

        t[i+1] = t[i] + h
        y1[i+1] = y1[i] + (k1_1 + 2*k2_1 + 2*k3_1 + k4_1)/6
        y2[i+1] = y2[i] + (k1_2 + 2*k2_2 + 2*k3_2 + k4_2)/6
        y[i] = y1[i]

    t2 = np.linspace(a, b, 1000)

    plt.plot(t, y, label='Numerical t-y')
    plt.plot(t2, np.cos(t2), label='Analytic t-y')
    plt.legend(loc='upper left')
    plt.show()

if __name__=='__main__':
    main()

