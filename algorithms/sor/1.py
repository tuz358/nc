#!/usr/bin/python3
#-*- coding: utf-8 -*-

import numpy as np


def SOR(A, b, omega=1.0):
    # A = L + D + U
    D = np.diagflat(np.diag(A))
    L = np.tril(A) - D
    U = np.triu(A) - D

    N = omega*np.linalg.inv(D + omega*L)
    # 反復行列
    M = np.dot(np.linalg.inv(D+omega*L), (1-omega)*D-omega*U)

    x = np.ones_like(b)
    x_new = np.zeros_like(b)

    i = 0
    eps = 1e10
    EPS = 1e-9
    while EPS < eps and i < 1e10:
        x_new = np.dot(M, x) + np.dot(N, b)
        eps = np.linalg.norm(x_new - x, np.inf)
        x = x_new
        i += 1

    return x_new

def main():
    A = np.array([[7, -1, 0, 1],
        [-1, 9, -2, 2],
        [0, -2, 8, -3],
        [1, 2, -3, 10],
        ])
    b = np.array([-5, 15, -10, 20])

    x = SOR(A, b, omega=1.2)
    print(x)
    print(np.linalg.inv(A).dot(b))

if __name__=='__main__':
    main()
