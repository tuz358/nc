#!/usr/bin/python3
#-*- coding: utf-8 -*-
#
# 時間積分法のまとめ
# 例として1D線形移流方程式を解く

import numpy as np
import pickle


# 1D移流方程式の右辺
def conv1D(t, u, c, dx):
    du = np.convolve(u, [3.0, -4.0, 1.0], 'valid')/(2.0*dx)
    return np.hstack([0.0, 0.0, -c*du])


# Euler法
def euler(func, T, y0, params=None):
    dt = (T[-1] - T[0])/len(T)
    y = np.zeros([len(T), len(y0)])
    y[0] = y0

    for i,t in enumerate(T[:-1]):
        y[i+1] = y[i] + dt*func(t,y[i], *params)
    return y

# 4次Runge-Kutta法
def RK4(func, T, y0, params=None):
    dt = (T[-1] - T[0])/len(T)
    y = np.zeros([len(T), len(y0)])
    y[0] = y0

    for i,t in enumerate(T[:-1]):
        k1 = func(t, y[i], *params)
        k2 = func(t+dt/2, y[i]+k1*dt/2, *params)
        k3 = func(t+dt/2, y[i]+k2*dt/2, *params)
        k4 = func(t+dt, y[i]+k3*dt, *params)
        y[i+1] = y[i] + (k1 + 2*k2 + 2*k3 + k4)*dt/6.0
    return y


# Constant
L = 20.0 # x軸長さ
N = 256 # 格子数(FFTでは2**nとなる値である必要がある)
dx = L/N
T = 10
dt = 1.0e-3
c = 1.0 # speed

x = np.linspace(0, L, N)
t = np.arange(0, T, dt)

# 初期条件
u0 = np.zeros_like(x)
u0[5:50] = 2.0

#u = euler(func=conv1D, T=t, y0=u0, params=(c,dx))
u = RK4(func=conv1D, T=t, y0=u0, params=(c,dx))
with open('result_u.pkl', 'wb') as f:
    pickle.dump(u, f)
