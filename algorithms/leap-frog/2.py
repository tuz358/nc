#!/usr/bin/python3
#-*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
import pickle
import sys


def f(x, y, a=1.0, mu=0.0, sigma=1.0): # 初期条件
    return a*np.exp(-((x-mu)**2 + (y-mu)**2)/(2.0/sigma**2))

# 2次元移流方程式(Leap-Frog法)
def main():
    a = 10; b = 10; c = 1.5
    dx = 0.1
    dy = 0.1
    dt = 0.01
    v = [2.0, 2.0]
    x = np.arange(0, a, dx)
    y = np.arange(0, b, dy)
    t = np.arange(0, c, dt)
    u = np.zeros([len(t), len(x), len(y)])

    # 初期条件
    X, Y = np.meshgrid(x, y)
    u[0] = f(X, Y, a=1.0, mu=3, sigma=1.0)

    r1 = v[0]*dt/dx; r2 = v[1]*dt/dy;
    for j in range(1, len(x)-1): # 初期条件
        for k in range(1, len(y)-1):
            u[1][j][k] = u[0][j][k]
            u[1][j][k] -= v[0]*r1*(u[0][j+1][k] - u[0][j][k])
            u[1][j][k] -= v[1]*r2*(u[0][j][k+1] - u[0][j][k])

    # 境界条件
    u[:][0][:], u[:][-1][:] = u[:][1][:], u[:][-2][:]
    u[:][:][0], u[:][:][-1] = u[:][:][1], u[:][:][-2]

    print('r1 = %lf' % (r1)) # 安定性解析
    print('r2 = %lf' % (r2)) # 安定性解析

    progress = (len(t)-1)*(len(x)-1)*(len(y)-1)
    cnt = 0
    for i in range(1, len(t)-1): # t
        for j in range(1, len(x)-1): # x
            for k in range(1, len(y)-1): # y
                u[i+1][j][k] = u[i-1][j][k]
                u[i+1][j][k] -= v[0]*r1*(u[i][j+1][k] - u[i][j-1][k])
                u[i+1][j][k] -= v[1]*r2*(u[i][j][k+1] - u[i][j][k-1])

                cnt += 1
                sys.stdout.write('\r{:.3f} %'.format(cnt/progress*100))
                sys.stdout.flush()

    print('')

    with open('leap-frog.pkl', 'wb') as result:
        pickle.dump(u, result)

if __name__=='__main__':
    main()

