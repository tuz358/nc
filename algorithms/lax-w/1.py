#!/usr/bin/python3
#-*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import sys

def gauss(x, a=1, mu=0, sigma=1):
    return a*np.exp(-(x - mu)**2 / (2*sigma**2))

# 1次元移流方程式(Lax-Wendroff法)
def main():
    a = 10; b = 5
    dx = 0.1
    dt = 0.001
    c = 10.0 # speed
    x = np.arange(0, a, dx)
    t = np.arange(0, b, dt)
    u = np.zeros([len(t), len(x)])

    # 初期条件
    u[0] = 10*gauss(x, mu=3.0);
    # ノイマン境界条件
    u[0][0] = u[0][1]; u[0][-1] = u[0][-2]

    print('dt/2dx = %lf' % (dt/(2*dx))) # 安定性解析

    r = c*dt/dx
    for i in range(len(t)-1): # t
        u[i][0] = u[i][1]; u[i][-1] = u[i][-2] # ノイマン境界条件
        for j in range(1,len(x)-1): # x
            u[i+1][j] = 0.5*r*(r-1)*u[i][j+1] + (1-r**2)*u[i][j] + 0.5*r*(r+1)*u[i][j-1]

    fig = plt.figure()
    frame = int(len(t)*0.1)
    def update(i):
        if i != 0:
            fig.clear()
        #plt.ylim(-2, 12)
        plt.plot(x, u[i], color='blue')
        sys.stdout.write('\rexporting {:.3f} %'.format(100*i/frame))
        sys.stdout.flush()


    ani = animation.FuncAnimation(fig, update, interval=1, frames=frame)
    ani.save('output.gif', writer='imagemagick')
    #fig.show()

if __name__=='__main__':
    main()

