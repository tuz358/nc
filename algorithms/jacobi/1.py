#!/usr/bin/python3
#-*- coding: utf-8 -*-

import numpy as np


def jacobi(A, b):
    D = np.diagflat(np.diag(A))
    D_inv = np.linalg.inv(D)
    L = np.tril(A) - D
    U = np.triu(A) - D

    x = np.ones_like(b)
    x_new = np.zeros_like(b)

    i = 0
    eps = 1e10
    EPS = 1e-9
    while EPS < eps and i < 1e10:
        x_new = np.dot(D_inv, b - np.dot(L+U, x))
        eps = np.linalg.norm(x_new - x, np.inf)
        x = x_new
        i += 1

    return x_new

def main():
    A = np.array([[7, -1, 0, 1],
        [-1, 9, -2, 2],
        [0, -2, 8, -3],
        [1, 2, -3, 10],
        ])
    b = np.array([-5, 15, -10, 20])

    x = jacobi(A, b)
    print(x)
    print(np.linalg.inv(A).dot(b))

if __name__=='__main__':
    main()
