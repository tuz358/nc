#!/usr/bin/python3
#-*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np


def f(x, y):
    return x + y

def main():
    a = 0
    b = 10
    n = 1000
    h = (b - a)/n
    x = np.zeros(n)
    y = np.zeros(n)
    x[0] = a
    y[0] = 1

    for i in range(n-1):
        x[i+1] = x[i] + h
        y[i+1] = y[i] + h*f(x[i], y[i])

    x2 = np.linspace(a, b, num=1000)
    y2 = np.zeros(1000)
    for i in range(1000):
        y2[i] = -x2[i]-1+2*np.exp(x2[i])

    plt.plot(x, y, label='Numerical')
    plt.plot(x2, y2, label='Analytic')
    plt.legend()
    plt.show()

if __name__=='__main__':
    main()

