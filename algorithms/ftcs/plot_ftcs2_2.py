#!/usr/bin/python3
#-*- coding: utf-8 -*-

import matplotlib.animation as animation
import matplotlib.pyplot as plt
import numpy as np
import pickle
import sys


# 2次元偏微分方程式(拡散方程式)
def main():
    x = np.arange(0, 10, 0.1)
    t = np.arange(0, 200, 0.05)

    with open('ftcs2_2.pkl', 'rb') as result:
        u = pickle.load(result)

    fig = plt.figure()
    frame = len(t)
    def update(i):
        if i != 0:
            fig.clear()
        plt.ylim(-15, 15)
        plt.plot(x, u[i], color='blue')

        sys.stdout.write('\rexporting {:.3f} %'.format(100*i/frame))
        sys.stdout.flush()

    ani = animation.FuncAnimation(fig, update, interval=1, frames=frame)
    fig.show()
    ani.save('output.gif', writer='imagemagick')

    print('')

if __name__=='__main__':
    main()

