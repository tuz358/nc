#!/usr/bin/python3
#-*- coding: utf-8 -*-

#import matplotlib.pyplot as plt
import numpy as np
import pickle


def gauss(x, a=1, mu=0, sigma=1):
    return a*np.exp(-(x - mu)**2 / (2*sigma**2))

# 偏微分方程式(波動方程式)
def main():
    a = 10; b = 200
    dx = 0.1
    dt = 0.05
    x = np.arange(0, a, dx)
    t = np.arange(0, b, dt)
    u = np.zeros([len(t), len(x)])

    # 境界条件
    u[:][0] = 0; u[:][-1] = 0

    # 初期条件
    u[0] = 10*gauss(x, mu=a/2);
    for j in range(1, len(x)-1):
        u[1][j] = u[0][j] + 0.5*((dt/dx)**2)*(u[0][j+1] -2*u[0][j] + u[0][j-1])

    print('(dt/dx)**2 = %lf' % (dt/dx)**2) # 安定性解析

    for i in range(1, len(t)-1): # t
        for j in range(1, len(x)-1): # x
            u[i+1][j] = 2*u[i][j] - u[i-1][j] + (dt/dx)**2*(u[i][j+1] -2*u[i][j] + u[i][j-1])

    with open('ftcs2_2.pkl', 'wb') as result:
        pickle.dump(u, result)

if __name__=='__main__':
    main()

