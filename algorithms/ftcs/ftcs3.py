#!/usr/bin/python3
#-*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
import pickle
import sys


def f(x, y, a=1.0, mu=0.0, sigma=1.0): # 初期条件
    return a*np.exp(-((x-mu)**2 + (y-mu)**2)/(2.0/sigma**2))

# 2次元偏微分方程式(拡散方程式)
def main():
    a = 5; b = 5; c = 3
    dx = 0.2
    dy = 0.2
    dt = 0.005
    x = np.arange(0, a, dx)
    y = np.arange(0, b, dy)
    t = np.arange(0, c, dt)
    u = np.zeros([int(c/dt), int(a/dx), int(b/dy)])

    X, Y = np.meshgrid(x, y)
    u[0] = f(X, Y, a = 40, mu=2.5)

    print('dt/(dx**2) = %lf' % (dt/(dx**2))) # 安定性解析
    print('dt/(dy**2) = %lf' % (dt/(dy**2))) # 安定性解析

    progress = (len(t)-1)*(len(x)-1)*(len(y)-1)
    cnt = 0
    for i in range(len(t)-1): # t
        for j in range(len(x)-1): # x
            for k in range(len(y)-1): # y
                u[i+1][j][k] = u[i][j][k]
                u[i+1][j][k] += dt/(dx**2)*(u[i][j+1][k] -2*u[i][j][k] + u[i][j-1][k])
                u[i+1][j][k] += dt/(dy**2)*(u[i][j][k+1] -2*u[i][j][k] + u[i][j][k-1])

                # ノイマン境界条件
                u[i][0][k]  = u[i][1][k]
                u[i][-1][k] = u[i][-2][k]
                u[i][j][0]  = u[i][j][1]
                u[i][j][-1] = u[i][j][-2]

                cnt += 1
                sys.stdout.write('\r{:.3f} %'.format(cnt/progress*100))
                sys.stdout.flush()

    print('')

    with open('ftcs3.pkl', 'wb') as result:
        pickle.dump(u, result)

if __name__=='__main__':
    main()

