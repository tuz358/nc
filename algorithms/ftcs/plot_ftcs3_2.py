#!/usr/bin/python3
#-*- coding: utf-8 -*-

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import pickle
import sys


# 2次元偏微分方程式(拡散方程式)
def main():
    x = np.arange(0, 30, 0.5)
    y = np.arange(0, 30, 0.5)
    t = np.arange(0, 15, 0.05)

    X, Y = np.meshgrid(x, y)

    with open('ftcs3_2.pkl', 'rb') as result:
        u = pickle.load(result)

    fig = plt.figure()
    axes = fig.gca(projection='3d')

    frame = len(t)
    def update(i):
        if i != 0:
            axes.clear()
        #axes.plot_surface(X, Y, u[i])
        axes.plot_wireframe(X[:-1], Y[:-1], u[i][:-1]) # 境界は外す
        axes.set_zlim(-1, 1)

        sys.stdout.write('\rexporting {:.3f} %'.format(100*i/frame))
        sys.stdout.flush()

        return axes

    ani = animation.FuncAnimation(fig, update, interval=1, frames=frame) # 1 ms
    #fig.show()
    ani.save('output.gif', writer='imagemagick')

    print('')

if __name__=='__main__':
    main()

