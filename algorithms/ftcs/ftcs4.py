#!/usr/bin/python3
#-*- coding: utf-8 -*-

from scipy.sparse import diags
import matplotlib.pyplot as plt
import numpy as np

'''
$ time ./ftcs2.py 
dt/(dx**2) = 0.100000

real	0m2.923s
user	0m2.713s
sys	0m0.044s

$ time ./ftcs4.py
dt/(dx**2) = 0.100000

real	0m0.694s
user	0m0.487s
sys	0m0.032s
$
'''


def gauss(x, a=1, mu=0, sigma=1):
    return a*np.exp(-(x - mu)**2 / (2*sigma**2))

# 偏微分方程式(拡散方程式)
def main():
    a = 10; b = 10
    dx = 0.1
    dt = 0.001
    x = np.arange(0, a, dx)
    t = np.arange(0, b, dt)
    u = np.zeros([len(t), len(x)])
    u[0] = 10*gauss(x, mu=a/2);

    r = dt/(dx**2)
    A = diags([r, 1-2*r, r], [-1, 0, 1], shape=(len(x),len(x))).toarray()
    B = np.zeros(len(x))

    print('dt/(dx**2) = %lf' % (dt/(dx**2))) # 安定性解析

    for i in range(len(t)-1): # t
        B[0] = u[i][0]; B[-1] = u[i][-1]
        u[i+1] = np.dot(A, u[i]) + r*B

        u[i][0] = u[i][1]; u[i][-1] = u[i][-2] # ノイマン条件

    for i in range(5):
        plt.plot(x, u[i*500], label=str(i))
    plt.plot(x, u[-2], label='last')
    plt.legend()
    plt.show()

if __name__=='__main__':
    main()

