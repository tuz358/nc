#!/usr/bin/python3
#-*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np


# 偏微分方程式(拡散方程式)
def main():
    a = 0
    b = 10
    c = 100
    n = 50
    n2 = 10000
    dx = (b - a)/n
    dt = 0.01
    x = np.linspace(a, b, n)
    t = np.arange(a, c, dt)
    u = np.zeros([n2, n])
    u[0] = 10;
    u[0][0] = 100; u[0][-1] = u[0][-2] # ディリクレ境界条件

    for i in range(n2-1): # t
        for j in range(n-1): # x
            u[i+1][j] = u[i][j] + dt/(dx*dx)*(u[i][j+1] -2*u[i][j] + u[i][j-1])

            u[i][0] = 100; u[i][-1] = u[i][-2] # ディリクレ境界条件

    for i in range(5):
        plt.plot(x, u[i*200], label=str(i))
    plt.legend()
    plt.show()

if __name__=='__main__':
    main()

