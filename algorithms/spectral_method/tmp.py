#!/usr/bin/python3
#-*- coding: utf-8 -*-
#
# Spectral method

#from scipy.integrate import solve_ivp
from numpy.fft import fft, rfft, irfft, fftfreq
import numpy as np
import pickle


# Constant
L = 10.0 # x軸長さ
T = 10
N = 256 # 格子数(FFTでは2**nとなる値である必要がある)
#m = N//2 + 1 # 波数の大きさ
m = N # 波数の大きさ
dt = 1.0e-3
dx = L/N

x = np.linspace(0, L, N)
t = np.arange(0, T, dt)
k = fftfreq(n=m, d=0.2)*m*2*np.pi/L # 波数

# Initial conds.
#u0 = np.cos(np.pi*x)
u0 = np.zeros_like(x)
u0[20:70] = 1


# RHS of equations(実空間)
def conv(t, u):
    #du = np.convolve(u, [3.0, -4.0, 1.0], 'valid')/(2*dx)
    du = np.convolve(u, [3.0, -4.0, 1.0], 'valid')/(2*dx)
    return np.hstack([0.0, 0.0, -du])

# RHS of equations(波数空間)
def conv_k(t, u):
    #v = fft(u) # フーリエ変換
    #return -1.0j*k * u
    v = fft(u) # フーリエ変換
    return -1.0j*k * v

def euler(func, T, y0):
    dt = (T[-1] - T[0])/len(T)
    y = np.zeros([len(T), len(y0)])
    #y = np.zeros([len(T), len(y0)], dtype=complex) # np.zeros([t.shape[0], y0.shape[0]])
    y[0] = y0

    for t in range(len(T[:-1])):
        #ret = func(t, y[t])
        #y[t+1] = y[t] + dt*ret
        y[t+1] = y[t] + dt*func(t,y[t])

    return y


#sol2 = solve_ivp(conv, [0, T], fft(u0), method='BDF', t_eval=t)

sol = euler(func=conv, T=t, y0=u0)
#sol = euler(func=conv_k, T=t, y0=fft(u0))

u = sol
with open('tmp_u.pkl', 'wb') as f:
    pickle.dump(u, f)
