#!/usr/bin/python3
#-*- coding: utf-8 -*-
#
# Pseudo spectral method

from scipy.integrate import solve_ivp
from numpy.fft import fft, rfft, irfft, fftfreq
import numpy as np
import pickle


# Constant
L = 2.0 # x軸長さ
T = 10
N = 256 # 格子数(FFTでは2**nとなる値である必要がある)
m = N//2 + 1 # 波数の大きさ
#m = N # 波数の大きさ
dt = 1.0e-2

x = np.linspace(0, L, N)
t = np.arange(0, T, dt)
k = fftfreq(n=m, d=1.0)*m*2*np.pi/L # 波数

# Parameters & Initial conds.
a = 1.0
b = 0.022**2
u0 = np.cos(np.pi*x)


# RHS of equations
def KdV(t, u):
    v = rfft(u) # フーリエ変換
    ux = irfft(1.0j*k*v) # 勾配
    conv = u*ux # convection項
    disp = irfft((1.0j*k)**3*v) # dispersion項
    return -a*conv - b*disp

sol = solve_ivp(KdV, [0, T], u0, method='Radau', t_eval=t)

u = sol
with open('KdV-1D_u.pkl', 'wb') as f:
    pickle.dump(u, f)
