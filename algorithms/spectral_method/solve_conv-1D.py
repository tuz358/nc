#!/usr/bin/python3
#-*- coding: utf-8 -*-
#
# スペクトル法により1D線形移流方程式を解く

from numpy.fft import fft, rfft, ifft, irfft, fftfreq
import numpy as np
import pickle



def RK4(func, T, y0, params=None):
    dt = T[1] - T[0]
    y = np.zeros([len(T), len(y0)], dtype=np.complex)
    y[0] = y0

    for i,t in enumerate(T[:-1]):
        k1 = func(t, y[i], *params)
        k2 = func(t+dt/2, y[i]+k1*dt/2, *params)
        k3 = func(t+dt/2, y[i]+k2*dt/2, *params)
        k4 = func(t+dt, y[i]+k3*dt, *params)
        y[i+1] = y[i] + (k1 + 2*k2 + 2*k3 + k4)*dt/6.0
    return y

# RHS of equations(実空間), 擬スペクトル法
def conv(t, u, k, a):
    v = rfft(u) # フーリエ変換
    ux = irfft(1.0j*k*v) # 勾配
    return -a*ux

# RHS of equations(波数空間)
def conv_k(t, u, k, a):
    return -a* 1.0j*k*u


# Constant
L = 2.0
N = 256
dt = 1.0e-4 # RK4は陽解法なので小さくとる
#m = N//2 + 1 # 波数の大きさ
m = N # 波数の大きさ

x = np.linspace(0, L, N)
t_span = np.arange(0, 1, dt)
k = fftfreq(n=m, d=1.0)*m*2*np.pi/L # 波数

# Parameters & Initial conds.
a = 1.0
b = 0.022**2
#u0 = np.cos(np.pi*x)
#u0 = np.zeros_like(x); u0[5:50] = 1.0 # 矩形波
u0 = np.exp(-((x-0.5)/1e-1)**2) # 正規分布

#u = RK4(func=conv, T=t_span, y0=u0, params=(k, a))
u = RK4(func=conv_k, T=t_span, y0=fft(u0), params=(k, a))

# 求めた解を波数空間から実空間に戻す
for i in range(len(u)):
    u[i] = ifft(u[i])
u = np.abs(u) #u = u.real # 虚部を捨てる


with open('conv-1D_u.pkl', 'wb') as f:
    pickle.dump(u, f)
