#!/usr/bin/python3
#-*- coding: utf-8 -*-

import numpy as np

m = 1.0
g = 9.81
dt = 0.1

# 斜方投射
def Fx(x, y, t):
    ans = 0.0
    return ans

def Fy(x, y, t):
    ans = -m*g
    return ans

def E(x, y, vx, vy):
    K = 0.5*m*(vx**2 + vy**2)
    U = m*g*y
    return K + U

def main():
    x = 0.0
    y = 0.0
    vx = 10.0
    vy = 10.0
    x_new = 0.0
    y_new = 0.0
    vx_new = 10.0
    vy_new = 10.0
    f = open('lfrog_2D.csv', 'w')

    for t in np.arange(0, 5, dt):
        f.write(f'{t},{vx},{vy},{x},{y},{E(x, y, vx, vy)}\n')

        x_new = x + vx*dt + Fx(x, y, t)/m*0.5*dt**2;
        y_new = y + vy*dt + Fy(x, y, t)/m*0.5*dt**2;
        vx_new = vx + 0.5*dt/m*(Fx(x, y, t) + Fx(x_new, y, t+dt))
        vy_new = vy + 0.5*dt/m*(Fy(x, y, t) + Fy(x, y_new, t+dt))

        x = x_new
        y = y_new
        vx = vx_new
        vy = vy_new

    f.close()

if __name__=='__main__':
    main()
