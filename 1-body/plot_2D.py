#!/usr/bin/python3
#-*- coding: utf-8 -*-

import matplotlib.animation as animation
import matplotlib.pyplot as plt
import numpy as np
import sys


def main():
    #data = np.loadtxt('euler_2D.csv', delimiter=',', dtype='float64')
    data = np.loadtxt('lfrog_2D.csv', delimiter=',', dtype='float64')
    t = data[:,0]
    vx = data[:,1]
    vy = data[:,2]
    x = data[:,3]
    y = data[:,4]

    fig = plt.figure()
    frame = len(t)
    def update(i):
        if i != 0:
            fig.clear()
        plt.xlim(0, 25)
        plt.ylim(0, 6)
        plt.plot(x[i], y[i], marker='.', markersize=10)
        plt.title(f't = {t[i]:.2f} [s], time step = {i}')

        sys.stdout.write(f'\rexporting {100*i/frame:.3f} %')
        sys.stdout.flush()

    ani = animation.FuncAnimation(fig, update, interval=1, frames=frame)
    fig.show()
    ani.save('output.gif', writer='imagemagick')
    print('')

if __name__=='__main__':
    main()
