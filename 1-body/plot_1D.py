#!/usr/bin/python3
#-*- coding: utf-8 -*-

import matplotlib.animation as animation
import matplotlib.pyplot as plt
import numpy as np
import sys


def main():
    data = np.loadtxt('lfrog_1D.csv', delimiter=',', dtype='float64')
    t = data[:,0]
    v = data[:,1]
    x = data[:,2]

    fig = plt.figure()
    frame = len(t)
    def update(i):
        if i != 0:
            fig.clear()
        plt.xlim(-5, 5)
        plt.plot(x[i], 0, marker='.', markersize=5)
        plt.title(f't = {t[i]:.2f} [s], time step = {i}')

        sys.stdout.write(f'\rexporting {100*i/frame:.3f} %')
        sys.stdout.flush()

    ani = animation.FuncAnimation(fig, update, interval=1, frames=frame)
    fig.show()
    ani.save('output.gif', writer='imagemagick')
    print('')

if __name__=='__main__':
    main()
