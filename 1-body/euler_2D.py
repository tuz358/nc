#!/usr/bin/python3
#-*- coding: utf-8 -*-

import numpy as np

m = 1.0
g = 9.81
dt = 0.1

# 斜方投射
def Fx(x, y, t):
    ans = 0.0
    return ans

def Fy(x, y, t):
    ans = -m*g
    return ans

def E(x, y, vx, vy):
    K = 0.5*m*(vx**2 + vy**2)
    U = m*g*y
    return K + U

def main():
    x = 0.0
    y = 0.0
    vx = 10.0
    vy = 10.0
    f = open('euler_2D.csv', 'w')

    for t in np.arange(0, 5, dt):
        f.write(f'{t},{vx},{vy},{x},{y},{E(x, y, vx, vy)}\n')

        x_new = x + vx*dt;
        y_new = y + vy*dt;
        vx_new = vx + dt*Fx(x, y, t)/m
        vy_new = vy + dt*Fy(x, y, t)/m

        x = x_new
        y = y_new
        vx = vx_new
        vy = vy_new

    f.close()

if __name__=='__main__':
    main()
