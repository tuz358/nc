#!/usr/bin/python3
#-*- coding: utf-8 -*-

# Navier-Stokes方程式(2次元正方キャビティー流れ、圧縮性、レギュラー格子)
# 基礎方程式 : 連続の式、NS式、状態方程式
# 時間項 : 前進差分
# 移流項 : 1次精度風上差分
# 拡散項 : 2次精度中心差分

import numpy as np
import pickle
import sys
import time

# Constant
a = 1.0
b = 1.0
c = 10
dx = 0.1
dy = 0.1
dt = 0.0005
mu = 0.1   # 粘性係数
R = 8.314472 # 気体定数 [J/(mol K)]
Mg = 2.0e-03 # H2ガスの分子量 [kg]
#Rg = R/Mg    # ガス定数 [J/(kg K)]
Rg = R/2.0   # ガス定数 [J/(g K)]
Rg = 0.1
T = 30.0     # 温度 [K]
v_wall = 1.0 # 壁の速度
C1 = dt/dx
C2 = dt/dy
C3 = 4.0*mu*dt/(3.0*dx**2)
C4 = mu*dt/(dy**2)
C5 = mu*dt/(12.0*dx*dy)

x = np.arange(0, a, dx)
y = np.arange(0, b, dy)
t = np.arange(0, c, dt)

u = np.zeros([len(t), len(x), len(y)])   # 流速x成分
v = np.zeros([len(t), len(x), len(y)])   # 流速y成分
P = np.ones([len(x), len(y)])            # 圧力
rho = np.ones([len(t), len(x), len(y)])  # 密度


def FDM():
    for i in range(1, len(t)-1): # t
        # Boundary Condition(壁上面をv_wall m/sで動かす)
        for k in range(len(y)): # left and right
            u[i][0][k], v[i][0][k] = 0.0, 0.0
            u[i][-1][k], v[i][-1][k] = 0.0, 0.0
            P[0][k] = P[1][k]; P[-1][k] = P[-2][k]
            rho[i][0][k] = rho[i][1][k]; rho[i][-1][k] = rho[i][-2][k]
        for j in range(len(x)): # bottom and top
            u[i][j][0], v[i][j][0] = 0.0, 0.0
            u[i][j][-1], v[i][j][-1] = v_wall, 0.0
            P[j][0] = P[j][1]; P[j][-1] = P[j][-2]
            rho[i][j][0] = rho[i][j][1]; rho[i][j][-1] = rho[i][j][-2]


        for j in range(1, len(x)-1): # x
            for k in range(1, len(y)-1): # y
                # 1. 連続の式から密度rhoを求める
                rho[i+1][j][k] = rho[i][j][k] - 0.5*C1*(u[i][j][k]*(rho[i][j+1][k] - rho[i][j-1][k]) + rho[i][j][k]*(u[i][j+1][k] - u[i][j-1][k])) - 0.5*C2*(v[i][j][k]*(rho[i][j][k+1] - rho[i][j][k-1]) + rho[i][j][k]*(v[i][j][k+1] - v[i][j][k-1]))
                # 2. 状態方程式から圧力Pを求める
                P[j][k] = rho[i][j][k]*Rg*T

        # 3. NS式から流束u, vを求める
        for j in range(1, len(x)-1): # x
            for k in range(1, len(y)-1): # y
                sys.stdout.write('\rtime step = {}, j = {}, k = {}'.format(i, j, k))
                sys.stdout.flush()

                if abs(u[i][j][k])*C1 < 1.0 or abs(v[i][j][k])*C2 < 1.0:
                    sys.stdout.write(', CFL: {}, {}'.format(abs(u[i][j][k])*C1, abs(v[i][j][k])*C2))
                    sys.stdout.flush()

                # 時間発展
                u[i+1][j][k] = u[i][j][k] \
                        - 0.5*C1*( (u[i][j][k] + abs(u[i][j][k]))*(u[i][j][k] - u[i][j-1][k]) + (u[i][j][k] - abs(u[i][j][k]))*(u[i][j+1][k] - u[i][j][k]) ) \
                        - 0.5*C2*( (v[i][j][k] + abs(v[i][j][k]))*(u[i][j][k] - u[i][j][k-1]) + (v[i][j][k] - abs(v[i][j][k]))*(u[i][j][k+1] - u[i][j][k]) ) \
                        + C3/rho[i][j][k]*(u[i][j+1][k] - 2*u[i][j][k] + u[i][j-1][k]) + C4/rho[i][j][k]*(u[i][j][k+1] - 2*u[i][j][k] + u[i][j][k-1]) \
                        + C5/rho[i][j][k]*(v[i][j+1][k+1] - v[i][j-1][k+1] - v[i][j+1][k-1] + v[i][j-1][k-1]) \
                        - C1/rho[i][j][k]*(P[j][k] - P[j-1][k])

                v[i+1][j][k] = v[i][j][k] \
                        - 0.5*C1*( (u[i][j][k] + abs(u[i][j][k]))*(v[i][j][k] - v[i][j-1][k]) + (u[i][j][k] - abs(u[i][j][k]))*(v[i][j+1][k] - v[i][j][k]) ) \
                        - 0.5*C2*( (v[i][j][k] + abs(v[i][j][k]))*(v[i][j][k] - v[i][j][k-1]) + (v[i][j][k] - abs(v[i][j][k]))*(v[i][j][k+1] - v[i][j][k]) ) \
                        + C3/rho[i][j][k]*(v[i][j+1][k] - 2*v[i][j][k] + v[i][j-1][k]) + C4/rho[i][j][k]*(v[i][j][k+1] - 2*v[i][j][k] + v[i][j][k-1]) \
                        + C5/rho[i][j][k]*(u[i][j+1][k+1] - u[i][j-1][k+1] - u[i][j+1][k-1] + u[i][j-1][k-1]) \
                        - C2/rho[i][j][k]*(P[j][k] - P[j][k-1])


def save():
    with open('1_u.pkl' , 'wb') as result:
        pickle.dump(u, result)
    with open('1_v.pkl' , 'wb') as result:
        pickle.dump(v, result)
    with open('1_P.pkl' , 'wb') as result:
        pickle.dump(P, result)
    with open('1_rho.pkl' , 'wb') as result:
        pickle.dump(rho, result)

def main():
    # 初期条件
    # u[0] = 0; v[0] = 0  <- t = 0(u, v宣言時に初期化済み)
    for j in range(1, len(x)-1): # x
        for k in range(1, len(y)-1): # y
            rho[1][j][k] = rho[0][j][k] - 0.5*C1*(u[0][j][k]*(rho[0][j+1][k] - rho[0][j-1][k]) + rho[0][j][k]*(u[0][j+1][k] - u[0][j-1][k])) - 0.5*C2*(v[0][j][k]*(rho[0][j][k+1] - rho[0][j][k-1]) + rho[0][j][k]*(v[0][j][k+1] - v[0][j][k-1]))

    for j in range(len(x)): # x
        for k in range(len(y)): # y
            P[j][k] = rho[0][j][k]*Rg*T

    try:
        FDM()
    except KeyboardInterrupt:
        save()
        print('')
    save()
    print('')

if __name__=='__main__':
        main()
