/*
 * 2次元正方キャビティ流れ(圧縮性、レギュラー格子)
 * 基礎方程式 : 連続の式、NS式、状態方程式
 * 時間項 : 前進差分
 * 移流項 : 1次精度風上差分
 * 拡散項 : 2次精度中心差分
 */

extern crate ndarray;

fn main() {
    let a = 1.0;
    let b = 1.0;
    let c = 10;
    let dx = 0.1;
    let dy = 0.1;
    let dt = 0.0005;
    let mu = 0.1; // 粘性係数
    let Rg = 0.1; // ガス定数
    let T = 30; // 温度
    let v_wall = 1.0; // 壁の速度
    let C1 = dt/dx;
    let C2 = dt/dy;
    let C3 = 4.0*mu*dt/(3.0*dx**2);
    let C4 = mu*dt/(dy**2);
    let C5 = mu*dt/(12.0*dx*dy);

    let mut u = ndarray::arr3();
    let mut v = ;
    let mut p = ;
    let mut rho = ;

    //println!("Hello World");
}
