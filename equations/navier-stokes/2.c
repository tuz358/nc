/*
 * Navier-Stokes方程式(2次元正方キャビティ流れ、圧縮性、レギュラー格子)
 * 基礎方程式: 連続の式、NS式、状態方程式
 * 時間項 : 前進差分
 * 移流項 : 1次精度風上差分
 * 拡散項 : 2次精度中心差分
 *
 * $ gcc 2.c -lm -std=c99
 * $ ./a.out
 */

#include <stdio.h>
#include <math.h>
#include <signal.h>

// Constant
#define Lx 1
#define Ly 1
#define c 20 // シミュレーション時間
#define dx 0.1
#define dy 0.1
#define dt 0.0001
#define Nx (int)(Lx/dx)
#define Ny (int)(Ly/dy)
#define Nt (int)(c/dt)
#define mu 0.1 // 粘性係数
//#define M 100.0 // 分子量
//#define R 100.0 // 気体定数
//#define Rg (double)(R/M)
#define Rg 0.1 // ガス定数
#define T 30.0 // 温度
#define v_wall 1.0 // 壁の速度
#define C1 (double)(dt/dx)
#define C2 (double)(dt/dy)
#define C3 (double)(4.0*mu*dt/(3.0*dx*dx))
#define C4 (double)(mu*dt/(dy*dy))
#define C5 mu*dt/(12.0*dx*dy)

double u[Nt][Nx][Ny];   // 流速x成分
double v[Nt][Nx][Ny];   // 流速y成分
double P[Nx][Ny];       // 圧力
double rho[Nt][Nx][Ny]; // 密度


void FDM(){
  // 初期条件(t = 0)
  for(int i = 0; i < Nt; i++){ // t
    for(int j = 0; j < Nx; j++){ // x
      for(int k = 0; k < Ny; k++){ // y
        u[i][j][k] = 0.0;
        v[i][j][k] = 0.0;
        P[j][k] = 1.0;
        rho[i][j][k] = 1.0;
      }
    }
  }
  for(int j = 1; j < Nx-1; j++){
    for(int k = 1; k < Ny-1; k++){
      rho[1][j][k] = rho[0][j][k] - 0.5*C1*(u[0][j][k]*(rho[0][j+1][k] - rho[0][j-1][k]) + rho[0][j][k]*(u[0][j+1][k] - u[0][j-1][k])) - 0.5*C2*(v[0][j][k]*(rho[0][j][k+1] - rho[0][j][k-1]) + rho[0][j][k]*(v[0][j][k+1] - v[0][j][k-1]));
    }
  }
  for(int j = 0; j < Nx; j++){ // x
    for(int k = 0; k < Ny; k++){ // y
      //u[0][j][k] = 0.0; v[0][j][k] = 0.0;
      P[j][k] = rho[0][j][k]*Rg*T;
    }
  }

  for(int i = 1; i < Nt-1; i++){ // t
    // Boundary condition(壁上面をv_wall m/sで動かす)
    for(int k = 0; k < Ny; k++){ // left and right
      u[i][0][k] = 0.0; v[i][0][k] = 0.0;
      u[i][Nx-1][k] = 0.0; v[i][Nx-1][k] = 0.0;
      P[0][k] = P[1][k]; P[Nx-1][k] = P[Nx-2][k];
      rho[i][0][k] = rho[i][1][k]; rho[i][Nx-1][k] = rho[i][Nx-2][k];
    }
    for(int j = 0; j < Nx; j++){ // bottom and top
      u[i][j][0] = 0.0; v[i][j][0] = 0.0;
      u[i][j][Ny-1] = v_wall; v[i][j][Ny-1] = 0.0;
      P[j][0] = P[j][1]; P[j][Ny-1] = P[j][Ny-2];
      rho[i][j][0] = rho[i][j][1]; rho[i][j][Ny-1] = rho[i][j][Ny-2];
    }

    for(int j = 1; j < Nx-1; j++){
      for(int k = 1; k < Ny-1; k++){
        // 1. 連続の式から密度rhoを求める
        rho[i+1][j][k] = rho[i][j][k] - 0.5*C1*(u[i][j][k]*(rho[i][j+1][k] - rho[i][j-1][k]) + rho[i][j][k]*(u[i][j+1][k] - u[i][j-1][k])) - 0.5*C2*(v[i][j][k]*(rho[i][j][k+1] - rho[i][j][k-1]) + rho[i][j][k]*(v[i][j][k+1] - v[i][j][k-1]));
        // 2. 状態方程式から圧力Pを求める
        P[j][k] = rho[i][j][k]*Rg*T;
      }
    }

    // 3. NS式から流束u, vを求める
    for(int j = 1; j < Nx-1; j++){ // x
      for(int k = 1; k < Ny-1; k++){ // y
        printf("\rtime step = %d, j = %2d, k = %2d", i, j, k);

        // 時間発展
        u[i+1][j][k] = u[i][j][k] 
          - 0.5*C1*( (u[i][j][k] + fabs(u[i][j][k]))*(u[i][j][k] - u[i][j-1][k]) + (u[i][j][k] - fabs(u[i][j][k]))*(u[i][j+1][k] - u[i][j][k]) )
          - 0.5*C2*( (v[i][j][k] + fabs(v[i][j][k]))*(u[i][j][k] - u[i][j][k-1]) + (v[i][j][k] - fabs(v[i][j][k]))*(u[i][j][k+1] - u[i][j][k]) )
          + C3/rho[i][j][k]*(u[i][j+1][k] - 2*u[i][j][k] + u[i][j-1][k]) + C4/rho[i][j][k]*(u[i][j][k+1] - 2*u[i][j][k] + u[i][j][k-1])
          + C5/rho[i][j][k]*(v[i][j+1][k+1] - v[i][j-1][k+1] - v[i][j+1][k-1] + v[i][j-1][k-1])
          - C1/rho[i][j][k]*(P[j][k] - P[j-1][k]);

        v[i+1][j][k] = v[i][j][k]
          - 0.5*C1*( (u[i][j][k] + fabs(u[i][j][k]))*(v[i][j][k] - v[i][j-1][k]) + (u[i][j][k] - fabs(u[i][j][k]))*(v[i][j+1][k] - v[i][j][k]) )
          - 0.5*C2*( (v[i][j][k] + fabs(v[i][j][k]))*(v[i][j][k] - v[i][j][k-1]) + (v[i][j][k] - fabs(v[i][j][k]))*(v[i][j][k+1] - v[i][j][k]) )
          + C3/rho[i][j][k]*(v[i][j+1][k] - 2*v[i][j][k] + v[i][j-1][k]) + C4/rho[i][j][k]*(v[i][j][k+1] - 2*v[i][j][k] + v[i][j][k-1])
          + C5/rho[i][j][k]*(u[i][j+1][k+1] - u[i][j-1][k+1] - u[i][j+1][k-1] + u[i][j-1][k-1])
          - C2/rho[i][j][k]*(P[j][k] - P[j][k-1]);

      }
    }

  }
}

void save(){
  FILE *fp1, *fp2, *fp3, *fp4;
  char u_csv[] = "u.csv";
  char v_csv[] = "v.csv";
  char P_csv[] = "P.csv";
  char rho_csv[] = "rho.csv";

  if((fp1 = fopen(u_csv, "w")) == NULL) printf("[ERR] : Can't open u.csv\n");
  if((fp2 = fopen(v_csv, "w")) == NULL) printf("[ERR] : Can't open v.csv\n");
  if((fp3 = fopen(P_csv, "w")) == NULL) printf("[ERR] : Can't open P.csv\n");
  if((fp4 = fopen(rho_csv, "w")) == NULL) printf("[ERR] : Can't open rho.csv\n");

  for(int i = 0; i < Nt; i++){
    for(int j = 0; j < Nx; j++){
      for(int k  = 0; k < Ny; k++){
        fprintf(fp1, "%lf,", u[i][j][k]);
        fprintf(fp2, "%lf,", v[i][j][k]);
        fprintf(fp4, "%lf,", rho[i][j][k]);
      }
    }
    fprintf(fp1, "\n");
    fprintf(fp2, "\n");
    fprintf(fp4, "\n");
  }
  for(int j = 0; j < Nx; j++){
    for(int k  = 0; k < Ny; k++){
        fprintf(fp3, "%lf,", P[j][k]);
    }
    fprintf(fp3, "\n");
  }

  fclose(fp1);
  fclose(fp2);
  fclose(fp3);
  fclose(fp4);
}

int main(void){
  printf("Nt = %d, Nx = %d, Ny = %d\n", Nt, Nx, Ny);

  FDM();

  save();

  printf("\n");

  return 0;
}
