#!/usr/bin/python3
#-*- coding: utf-8 -*-

# Navier-Stokes方程式(2次元、非圧縮性、キャビティー流れ、レギュラー格子)
# Poisson方程式Solver : SOR method
# 時間項 : 前進差分
# 移流項 : 1次精度風上差分
# 拡散項 : 2次精度中心差分

import numpy as np
import pickle
import sys
import time

# Constant
a = 1.0
b = 1.0
c = 20
dx = 0.05
dy = 0.05
dt = 0.001
Re = 100 # レイノルズ数
EPS = 1.0e-04
v_wall = 5.0 # 壁の速度
C1 = dt/dx
C2 = dt/dy
C3 = dt/(dx**2)/Re
C4 = dt/(dy**2)/Re
r5 = 2*(dx**2 + dy**2)
r6 = (dx*dy)**2/r5

x = np.arange(0, a, dx)
y = np.arange(0, b, dy)
t = np.arange(0, c, dt)

u = np.zeros([len(t), len(x), len(y)])   # 流速x成分
v = np.zeros([len(t), len(x), len(y)])   # 流速y成分
P = np.ones([len(x), len(y)])            # 圧力
P_old = 0                                # ノルム計算用バッファ

omega = 1.8 # 過緩和パラメータ

def f(i, j, k): # R.H.S. of Poisson's equation(非同次項)
    ans  = -(u[i][j+1][k]**2 - 2*u[i][j][k]**2 + u[i][j-1][k]**2)/(dx**2) # ans  = -((u[i][j+1][k] - u[i][j][k])/dx)**2
    ans += -(v[i][j][k+1]**2 - 2*v[i][j][k]**2 + v[i][j][k-1]**2)/(dy**2) # ans += -((v[i][j][k+1] - v[i][j][k])/dy)**2

    ans += -((u[i][j][k+1] - u[i][j][k-1])*(v[i][j+1][k] - v[i][j-1][k]) + (u[i][j+1][k] - u[i][j-1][k])*(v[i][j][k+1] - v[i][j][k-1]) \
            + u[i][j][k]*(v[i][j+1][k+1] - v[i][j-1][k+1] - v[i][j+1][k-1] + v[i][j-1][k-1]) \
            + v[i][j][k]*(u[i][j+1][k+1] - u[i][j-1][k+1] - u[i][j+1][k-1] + u[i][j-1][k-1]))/(2*dx*dy)
    # ans += -(u[i][j+1][k+1]*v[i][j+1][k+1] - u[i][j+1][k-1]*v[i][j+1][k-1] - u[i][j-1][k+1]*v[i][j-1][k+1] + u[i][j-1][k-1]*v[i][j-1][k-1])/(2*dx*dy)
    # ans += -(u[i][j+1][k]*v[i][j][k+1] - u[i][j-1][k]*v[i][j][k-1] - u[i][j-1][k]*v[i][j][k+1] + u[i][j-1][k]*v[i][j][k-1])/(2*dx*dy)

    ans += -((u[i][j+1][k] - u[i][j-1][k])/dx + (v[i][j][k+1] - v[i][j][k-1])/dy)/(2*dt)
    return ans

def poisson(i):
    # Boundary condition
    for k in range(len(y)): # left and right
        P[0][k] = P[1][k]
        P[-1][k] = P[-2][k]
    for j in range(len(x)): # bottom and top
        P[j][0] = P[j][1]
        P[j][-1] = P[j][-2]

    eps = 1.0
    P_max = 1.0e-10
    while EPS < eps:
        eps = 0.0
        now_max = 0.0
        for j in range(1, len(x)-1): # x
            for k in range(1, len(y)-1): # y
                P_old = P[j][k]
                P[j][k] = P[j][k] + omega*( f(i, j, k)*r6 + (dy**2*(P[j+1][k] + P[j-1][k]) + dx**2*(P[j][k+1] + P[j][k-1]))/r5 - P[j][k])
                # P[j][k] = f(i, j, k)*r6 + (dy**2*(P[j+1][k] + P[j-1][k]) + dx**2*(P[j][k+1] + P[j][k-1]))/r5 # Jacobi method

                err = abs(P[j][k] - P_old)
                eps = err if eps < err else eps

                now_max = abs(P[j][k]) if now_max < abs(P[j][k]) else now_max

        P_max = now_max if P_max < now_max else P_max
        #P_max = np.max(abs(P)) if P_max < np.max(abs(P)) else P_max
        #if P_max < np.max(abs(P)):
        #    P_max = np.max(P)
        eps /= P_max # 現在の最大圧力で規格化

def FDM():
    for i in range(1, len(t)): # t
        # Boundary Condition(壁上面をv_wall m/sで動かす)
        for k in range(len(y)):
            u[i][0][k], v[i][0][k] = 0.0, 0.0 # left
            u[i][-1][k], v[i][-1][k] = 0.0, 0.0 # right
        for j in range(len(x)):
            u[i][j][0], v[i][j][0] = 0.0, 0.0 # bottom
            u[i][j][-1], v[i][j][-1] = v_wall, 0.0 # top

        poisson(i)

        for j in range(1, len(x)-1): # x
            for k in range(1, len(y)-1): # y
                sys.stdout.write('\rtime step = {}, j = {}, k = {}'.format(i, j, k))
                sys.stdout.flush()


                # 時間発展
                u[i+1][j][k] = u[i][j][k] - C1*u[i][j][k]*(u[i][j+1][k] - u[i][j-1][k])/2.0 - C2*v[i][j][k]*(u[i][j][k+1] - u[i][j][k-1])/2.0 \
                        + C3*(u[i][j+1][k] - 2*u[i][j][k] + u[i][j-1][k]) + C4*(u[i][j][k+1] - 2*u[i][j][k] + u[i][j][k-1]) \
                        - C1*(P[j][k] - P[j-1][k])
                v[i+1][j][k] = v[i][j][k] - C1*u[i][j][k]*(v[i][j+1][k] - v[i][j-1][k])/2.0 - C2*v[i][j][k]*(v[i][j][k+1] - v[i][j][k-1])/2.0 \
                        + C3*(v[i][j+1][k] - 2*v[i][j][k] + v[i][j-1][k]) + C4*(v[i][j][k+1] - 2*v[i][j][k] + v[i][j][k-1]) \
                        - C2*(P[j][k] - P[j][k-1])


def save():
    with open('1_u.pkl' , 'wb') as result:
        pickle.dump(u, result)
    with open('1_v.pkl' , 'wb') as result:
        pickle.dump(v, result)
    with open('1_P.pkl' , 'wb') as result:
        pickle.dump(P, result)

def main():
    # 初期条件
    # u[0] = 0; v[0] = 0  <- t = 0(u, v宣言時に初期化済み)

    try:
        FDM()
    except KeyboardInterrupt:
        save()
        print('')
    save()
    print('')

if __name__=='__main__':
        main()
