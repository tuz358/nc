#!/usr/bin/python3
#-*- coding: utf-8 -*-

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import pickle
import sys

def main():
    x = np.arange(0, 1, 0.05)
    y = np.arange(0, 1, 0.05)
    t = np.arange(0, 5, 0.001)

    with open('1_u.pkl', 'rb') as ret:
        u = pickle.load(ret)
    with open('1_v.pkl', 'rb') as ret:
        v = pickle.load(ret)
    with open('1_P.pkl', 'rb') as ret:
        P = pickle.load(ret)

    fig = plt.figure()
    frame = len(t)
    def update(i):
        if i != 0:
            fig.clear()
        for j in range(len(x)):
            for k in range(len(y)):
                V_x = u[i][j][k]
                V_y = v[i][j][k]
                #print('V = [{}, {}]'.format(V_x, V_y))
                plt.quiver(j, k, V_x, V_y, angles='xy', scale_units='xy', scale=1)

                sys.stdout.write('\rtime step = {}'.format(i))
                sys.stdout.flush()
        return plt

    ani = animation.FuncAnimation(fig, update, interval=1, frames=frame)
    fig.show()

    #plt.imshow(P, cmap='coolwarm')
    #pp = plt.colorbar(orientation='vertical')
    #plt.show()

if __name__=='__main__':
    main()
