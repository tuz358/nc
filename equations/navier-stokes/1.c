// Navier-Stokes方程式(2次元正方キャビティ流れ、非圧縮、レギュラー格子)
// Poisson方程式solver: SOR method
// 時間項 : 前進差分
// 移流項 : 中心差分
// 拡散項 : 2次精度中心差分
//
// $ gcc 1.c -lm -std=c99
// $ ./a.out
#include <stdio.h>
#include <math.h>
#include <signal.h>

// Constant
#define Lx 1
#define Ly 1
#define T 20 // シミュレーション時間
#define dx 0.05
#define dy 0.05
#define dt 0.001
#define Nx (int)(Lx/dx)
#define Ny (int)(Ly/dy)
#define Nt (int)(T/dt)
#define Re 100.0 // レイノルズ数
#define EPS 1.0e-04
#define v_wall 5.0 // 壁の速度
#define C1 (double)(dt/dx)
#define C2 (double)(dt/dy)
#define C3 (double)(dt/(dx*dx)/Re)
#define C4 (double)(dt/(dy*dy)/Re)
#define C5 2.0*(dx*dx + dy*dy)
#define C6 (dx*dx)*(dy*dy)/C5
#define omega 1.8 // 過緩和パラメータ

double u[Nt][Nx][Ny]; // 流速x成分
double v[Nt][Nx][Ny]; // 流速y成分
double P[Nx][Ny];     // 圧力


double f(int i, int j, int k){
  /* R.H.S. of Poisson's epuation */
  double ans = 0.0;
  ans  = -(u[i][j+1][k]*u[i][j+1][k] - 2.0*u[i][j][k]*u[i][j][k] + u[i][j-1][k]*u[i][j-1][k])/(dx*dx);
  ans += -(v[i][j][k+1]*v[i][j][k+1] - 2.0*v[i][j][k]*v[i][j][k] + v[i][j][k-1]*v[i][j][k-1])/(dy*dy);
  
  ans += -((u[i][j][k+1] - u[i][j][k-1])*(v[i][j+1][k] - v[i][j-1][k]) + (u[i][j+1][k] - u[i][j-1][k])*(v[i][j][k+1] - v[i][j][k-1])
      + u[i][j][k]*(v[i][j+1][k+1] - v[i][j-1][k+1] - v[i][j+1][k-1] + v[i][j-1][k-1]) 
      + v[i][j][k]*(u[i][j+1][k+1] - u[i][j-1][k+1] - u[i][j+1][k-1] + u[i][j-1][k-1]))/(2.0*dx*dy);

  ans += -((u[i][j+1][k] - u[i][j-1][k])/dx + (v[i][j][k+1] - v[i][j][k-1])/dy)/(2.0*dt);
  return ans;
}

void poisson(int i){
  // Boundary condition
  for(int k = 0; k < Ny; k++){ // left and right
    P[0][k] = P[1][k];
    P[Nx-1][k] = P[Nx-2][k];
  }
  for(int j = 0; j < Nx; j++){ // bottom and top
    P[j][0] = P[j][1];
    P[j][Ny-1] = P[j][Ny-2];
  }

  double eps = 1.0;
  double P_max = 1.0e-10;
  double P_old = 0.0;
  while(EPS < eps){
    eps = 0.0;
    double now_max = 0.0;
    for(int j = 1; j < Nx-1; j++){
      for(int k = 1; k < Ny-1; k++){
        P_old = P[j][k];
        P[j][k] = P[j][k] + omega*( f(i, j, k)*C6 + (dy*dy*(P[j+1][k] + P[j-1][k]) + dx*dx*(P[j][k+1] + P[j][k-1]))/C5 - P[j][k]);

        double err = fabs(P[j][k] - P_old);
        eps = eps < err ? err : eps;
        now_max = now_max < fabs(P[j][k]) ? fabs(P[j][k]) : now_max;
      }
    }

    if(P_max < now_max) P_max = now_max;
    eps /= P_max;
  }
}

void FDM(){
  // 初期条件(t = 0)
  for(int i = 0; i < Nt; i++){
    for(int j = 0; j < Nx; j++){
      for(int k = 0; k < Ny; k++){
        u[i][j][k] = 0.0;
        v[i][j][k] = 0.0;
        P[j][k] = 1.0;
      }
    }
  }

  for(int i = 1; i < Nt-1; i++){ // t
    // Boundary condition(壁上面をv_wall m/sで動かす)
    for(int k = 0; k < Ny; k++){
      u[i][0][k] = 0.0; v[i][0][k] = 0.0; // left
      u[i][Nx-1][k] = 0.0; v[i][Nx-1][k] = 0.0; // right
    }
    for(int j = 0; j < Nx; j++){
      u[i][j][0] = 0.0; v[i][j][0] = 0.0; // bottom
      u[i][j][Ny-1] = v_wall; v[i][j][Ny-1] = 0.0; // top
    }

    poisson(i);

    for(int j = 1; j < Nx-1; j++){
      for(int k = 1; k < Ny-1; k++){
        printf("\rtime step = %d, j = %2d, k = %2d", i, j, k);

        // 時間発展
        u[i+1][j][k] = u[i][j][k] - 0.5*C1*u[i][j][k]*(u[i][j+1][k] - u[i][j-1][k]) - 0.5*C2*v[i][j][k]*(u[i][j][k+1] - u[i][j][k-1])
          + C3*(u[i][j+1][k] - 2.0*u[i][j][k] + u[i][j-1][k]) + C4*(u[i][j][k+1] - 2.0*u[i][j][k] + u[i][j][k-1]) \
          - C1*(P[j][k] - P[j-1][k]);
        
        v[i+1][j][k] = v[i][j][k] - 0.5*C1*u[i][j][k]*(v[i][j+1][k] - v[i][j-1][k]) - 0.5*C2*v[i][j][k]*(v[i][j][k+1] - v[i][j][k-1])
          + C3*(v[i][j+1][k] - 2.0*v[i][j][k] + v[i][j-1][k]) + C4*(v[i][j][k+1] - 2.0*v[i][j][k] + v[i][j][k-1])
          - C2*(P[j][k] - P[j][k-1]);
      }
    }

  }
}

void save(){
  FILE *fp1, *fp2, *fp3;
  char u_csv[] = "u.csv";
  char v_csv[] = "v.csv";
  char P_csv[] = "P.csv";

  if((fp1 = fopen(u_csv, "w")) == NULL) printf("[ERR] : Can't open u.csv\n");
  if((fp2 = fopen(v_csv, "w")) == NULL) printf("[ERR] : Can't open v.csv\n");
  if((fp3 = fopen(P_csv, "w")) == NULL) printf("[ERR] : Can't open P.csv\n");

  for(int i = 0; i < Nt; i++){
    for(int j = 0; j < Nx; j++){
      for(int k  = 0; k < Ny; k++){
        fprintf(fp1, "%lf,", u[i][j][k]);
        fprintf(fp2, "%lf,", v[i][j][k]);
      }
    }
    fprintf(fp1, "\n");
    fprintf(fp2, "\n");
  }
  for(int j = 0; j < Nx; j++){
    for(int k  = 0; k < Ny; k++){
        fprintf(fp3, "%lf,", P[j][k]);
    }
    fprintf(fp3, "\n");
  }

  fclose(fp1);
  fclose(fp2);
  fclose(fp3);
}

int main(void){
  printf("Nt = %d, Nx = %d, Ny = %d\n", Nt, Nx, Ny);

  FDM();

  save();

  printf("\n");

  return 0;
}
