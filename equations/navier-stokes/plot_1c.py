#!/usr/bin/python3
#-*- coding: utf-8 -*-

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
import sys

def main():
    with open('u.csv', 'rb') as ret:
        data = ret.read().decode('utf-8').splitlines()
        data = [d[:-1].split(',') for d in data]
        u = np.array(data, dtype=np.float64)
        len_x = int(np.sqrt(len(data[0])))
        len_y = int(np.sqrt(len(data[:][0])))
        u = u.reshape(len(data), len_x, len_y)
    with open('v.csv', 'rb') as ret:
        data = ret.read().decode('utf-8').splitlines()
        data = [d[:-1].split(',') for d in data]
        v = np.array(data, dtype=np.float64)
        len_x = int(np.sqrt(len(data[0])))
        len_y = int(np.sqrt(len(data[:][0])))
        v = v.reshape(len(data), len_x, len_y)
    with open('P.csv', 'rb') as ret:
        data = ret.read().decode('utf-8').splitlines()
        data = [d[:-1].split(',') for d in data]
        P = np.array(data, dtype=np.float64)

    #t = range(len(u))
    t = int(sys.argv[1])
    for j in range(len_x):
        for k in range(len_y):
            V_x = u[t][j][k]
            V_y = v[t][j][k]
            #print('j = {}, k = {}, V = [{}, {}]'.format(j, k, V_x, V_y))
            plt.quiver(j, k, V_x, V_y, angles='xy', scale_units='xy', scale=1)

    plt.imshow(P, cmap='coolwarm')
    pp = plt.colorbar(orientation='vertical')
    plt.show()

if __name__=='__main__':
    main()
