#!/usr/bin/python3
#-*- coding: utf-8 -*-
#
# 1次元移流方程式
# 風上差分

import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np


def f(x):
    '''矩形波'''
    if 5.0 < x < 10.0:
        return 2.0
    else:
        return 0.0

def main():
    a = 20; b = 5
    dx = 0.1
    dt = 0.001
    c = 10.0 # speed
    x = np.arange(0, a, dx)
    t = np.arange(0, b, dt)
    u = np.zeros([len(t), len(x)])
    exact = np.zeros([len(t), len(x)])

    # 初期条件
    for i in range(len(x)):
        u[0][i] = f(x[i])
        exact[0][i] = f(x[i])
    # 境界条件(Dirichlet)
    u[0][0] = 0; u[0][-1] = 0

    for i in range(len(t)-1): # t
        u[i][0] = 0; u[i][-1] = 0 # Dirichlet BC
        for j in range(1, len(x)-1): # x
            u[i+1][j] = u[i][j] - 0.5*dt/dx*( (c+abs(c))*(u[i][j]-u[i][j-1]) ) + 0.5*dt/dx*( (c-abs(c))*(u[i][j+1]-u[i][j]) )
            exact[i][j] = f(x[j] - c*t[i])

    fig = plt.figure()
    frame = len(t)
    def update(i):
        if i != 0:
            fig.clear()
        plt.ylim([-2, 4])
        plt.plot(x, u[i], color='blue', label='Upwind')
        plt.plot(x, exact[i], color='black', label='exact')
        plt.title('time step = {}'.format(i))
        plt.legend()

    ani = animation.FuncAnimation(fig, update, interval=1, frames=frame)
    fig.show()
    ani.save('output.gif', writer='imagemagick')

if __name__=='__main__':
    main()

