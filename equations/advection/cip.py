#!/usr/bin/python3
#-*- coding: utf-8 -*-
#
# 1次元非線形移流方程式
# CIP法(移流フェイズと非移流フェイズに分けて解く)

import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np


def f(x):
    '''矩形波'''
    if 5.0 < x < 10.0:
        return 2.0
    else:
        return 0.0

def df(x):
    '''矩形波の微分'''
    if x == 5.0:
        return 2.0
    elif x == 10.0:
        return -2.0
    else:
        return 0.0

def main():
    dx = 0.1
    dt = 0.001
    c = 10.0 # speed
    x = np.arange(0, 20, dx)
    t = np.arange(0, 5, dt)
    u = np.zeros([len(t), len(x)])
    exact = np.zeros([len(t), len(x)])

    # for CIP method
    g = np.zeros([len(t), len(x)]) # g = du/dt
    zeta = -c*dt

    # 初期条件
    for i in range(len(x)):
        u[0][i] = f(x[i])
        exact[0][i] = f(x[i])
        # g[0][i] = df(x[i]) <- ありでもなしでも動いた
    # 境界条件(Dirichlet)
    u[0][0] = 0; u[0][-1] = 0

    for i in range(len(t)-1): # t
        u[i][0] = 0; u[i][-1] = 0 # Dirichlet BC
        for j in range(1, len(x)-1): # x
            # CIP法のための係数計算
            C1 = (g[i][j] + g[i][j-1])/(dx**2) - 2.0*(u[i][j] - u[i][j-1])/(dx**3)
            C2 = 3.0*(u[i][j-1] - u[i][j])/(dx**2) + (2.0*g[i][j] + g[i][j-1])/dx

            # 時間発展
            u[i+1][j] = C1*zeta**3 + C2*zeta**2 + g[i][j]*zeta + u[i][j]
            g[i+1][j] = 3.0*C1*zeta**2 + 2.0*C2*zeta + g[i][j]
            exact[i][j] = f(x[j] - c*t[i])

    fig = plt.figure()
    frame = len(t)
    def update(i):
        if i != 0:
            fig.clear()
        plt.ylim([-2, 4])
        plt.plot(x, u[i], color='blue', label='CIP')
        plt.plot(x, exact[i], color='black', label='exact')
        plt.title('time step = {}'.format(i))
        plt.legend()

    ani = animation.FuncAnimation(fig, update, interval=1, frames=frame)
    fig.show()
    ani.save('output.gif', writer='imagemagick')

if __name__=='__main__':
    main()

