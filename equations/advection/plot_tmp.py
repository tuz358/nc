#!/usr/bin/python3
#-*- coding: utf-8 -*-

import matplotlib.animation as animation
import matplotlib.pyplot as plt
import numpy as np
import pickle
import sys


with open(sys.argv[1], 'rb') as f:
    u = pickle.load(f) # result from solve_ivp

fig, ax = plt.subplots()
ims = []
x = np.linspace(0, 1, u.shape[1])
for t in range(u.shape[0])[::50]:
    im = ax.plot(20*x, u[t], c='b')
    ax.set_xlabel("x")
    ax.set_ylabel("u")
    im.append(ax.text(0, 2.7, f'time step = {t}', fontsize=15))

    ims.append(im)
    #sys.stdout.write('\rexporting {:.3f} %'.format(100 * t/len(u.t)))
    #sys.stdout.flush()

#print('')
ani = animation.ArtistAnimation(fig, ims, interval=50, repeat=True, repeat_delay=None, blit=False)
ani.save(f'{sys.argv[1].rsplit(".")[0]}_out.gif', writer='pillow')
#plt.show()
