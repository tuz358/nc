module PoissonSolver
  using LinearAlgebra
  using SparseArrays
  using DataFrames
  using CSV

  # Constant
  ϵ0 = 8.85e-12 # 真空中の誘電率
  EPS = 1.0e-5 # 収束判定(Threshold)
  ω = 1.8 # 過緩和パラメータ

  function solve(filename)
    rho = Matrix{Float32}(DataFrame(CSV.File(filename, header=0)))
    H = size(rho)[1] #100x100
    dx = 1.0/H

    E = Matrix{Int32}(I, H, H)
    J = diagm(1 => ones(H-1), -1 => ones(H-1))

    A = Matrix{Float32}(-4kron(E, E) + kron(J, E) + kron(E, J))
    D = Diagonal(A)
    
    L = LowerTriangular(A) - D
    U = UpperTriangular(A) - D
    b = sparse(-dx^2 / ϵ0 * reshape(rho, length(rho), 1))
    
    tmp = Matrix{Float32}(inv(D + ω*L))
    M = sparse(Matrix{Float32}(tmp * ((1-ω)D - ω*U)))
    N = sparse(Matrix{Float32}(ω * tmp))

    ϕnew = sparse(Array{Float32}(zeros(H*H)))
    ϕ = sparse(Array{Float32}(zeros(H*H)))

    i = 0
    eps = 1.0
    while EPS < eps
      if i % 100 == 0
        println("iteration : $i, eps = $eps")
      end
    
      ϕnew = M*ϕ + N*b
      eps = norm(ϕnew - ϕ, Inf)
    
      ϕ = ϕnew
      i += 1
    end
    println("iteration : $i, eps = $eps")

    return reshape(ϕnew, H, H)
  end
end
