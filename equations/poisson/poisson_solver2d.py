#!/usr/bin/python3
#-*- coding: utf-8 -*-

from scipy.sparse import diags, csc_matrix, linalg
import numpy as np
import sys


# Constant
e0 = 8.85e-12 # 真空中の誘電率
EPS = 1.0e-05


# 2次元Poisson方程式(行列化SOR, scipyによる高速化)
class PoissonSolver2D(object):
    def __init__(self, rho):
        self.rho = rho
        self.H = self.rho.shape[0]
        self.dx = 1/self.H
        self.omega = 1.8

        I = np.eye(self.H)
        J = diags([1, 1], [-1, 1], shape=(self.H, self.H)).toarray()

        A = -4*np.kron(I, I) + np.kron(J, I) + np.kron(I, J)
        D = np.diagflat(np.diag(A))
        D_inv = np.reciprocal(D, where=D!=0) # D_inv = np.linalg.inv(D)と等価(Dは対角行列のため)
        L = np.tril(A) - D
        U = np.triu(A) - D
        b = -self.dx**2/e0 * self.rho.flatten() # b = -rho.flatten()/e0

        A = csc_matrix(A)
        D = csc_matrix(D)
        L = csc_matrix(L)
        U = csc_matrix(U)
        b = csc_matrix(b).T

        tmp = linalg.inv(D + self.omega*L)
        M = np.dot(tmp, (1-self.omega)*D - self.omega*U)
        N = self.omega*tmp

        self.M = csc_matrix(M)
        self.N = csc_matrix(N)
        self.b = b

    def solve(self):
        phi_new = csc_matrix(np.zeros([self.H, self.H]).flatten()).T
        phi = csc_matrix(np.zeros([self.H, self.H]).flatten()).T

        cnt = 0; eps = 1.0
        while EPS < eps:
            if cnt % 100 == 0:
                print(f'cnt = {cnt}, eps = {eps:e}')

            phi_new = self.M.dot(phi) + self.N.dot(self.b)

            eps = np.linalg.norm(phi_new.toarray() - phi.toarray(), np.inf)
            phi = phi_new
            cnt += 1

        print(f'cnt = {cnt}, eps = {eps:e}')
        phi_new = phi_new.toarray()
        return phi_new.reshape(self.H, self.H)


if __name__=='__main__':
    rho = np.loadtxt('input_rho.txt')

    solver = PoissonSolver2D(rho)
    result = solver.solve()

    np.savetxt('result.txt', result)
