#!/usr/bin/python3
#-*- coding: utf-8 -*-

from scipy.sparse import diags, lil_matrix, linalg
import matplotlib.pyplot as plt
import numpy as np
import sys

# Constant
e0 = 8.85e-12 # 真空中の誘電率
EPS = 1.0e-05
a = 1
h = 0.01 # 空間刻み幅
#N = int(a/h) - 2
N = int(a/h)
omega = 1.8

# 2次元Poisson方程式(行列化Gauss-Seidel, scipyによる高速化)
#@profile
def poisson_solver2D():
    eps = 1.0

    phi_new = np.zeros([N, N]).flatten()
    phi = np.zeros_like(phi_new)
    rho = np.zeros([N, N])

    print('h**2 = %lf' % (h**2))
    # 電荷を配置
    for j in range(N): # x
        for k in range(N): # y
            if ((N/2-j)**2 + (N/2-k)**2)*h**2 < 0.05**2:
                rho[j][k] = 1.0e-08
    np.savetxt("input.txt", rho)

    I = np.eye(N)
    J = diags([1, 1], [-1, 1], shape=(N, N)).toarray()

    A = -4*np.kron(I, I) + np.kron(J, I) + np.kron(I, J)
    L = np.tril(A)
    U = A - L
    b = -h**2/e0 * rho.flatten() # b = -rho.flatten()/e0

    A.astype(np.float32)
    L.astype(np.float32)
    U.astype(np.float32)
    b.astype(np.float32)

    A = lil_matrix(A).tocsc()
    L = lil_matrix(L).tocsc()
    U = lil_matrix(U).tocsc()
    b = lil_matrix(b).T.tocsc()

    L_inv = linalg.inv(L)
    L_inv = lil_matrix(L_inv).tocsc()
    L_inv.astype(np.float32)

    phi_new = lil_matrix(phi_new).T.tocsc()
    phi = lil_matrix(phi).T.tocsc()

    cnt = 0
    while EPS < eps:
        if cnt % 100 == 0:
            print(f'cnt = {cnt}, eps = {eps:e}')

        phi_new = L_inv.dot(b - U.dot(phi))

        eps = np.linalg.norm(phi_new.toarray() - phi.toarray(), np.inf)
        phi = phi_new
        cnt += 1

    print(f'cnt = {cnt}, eps = {eps:e}')
    phi_new = phi_new.toarray()
    return phi_new


if __name__=='__main__':
    result = poisson_solver2D()

    plt.imshow(result.reshape(N, N), cmap='inferno')
    pp = plt.colorbar(orientation='vertical')
    pp.set_label('phi [V]')
    plt.show()
