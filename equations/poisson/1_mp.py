#!/usr/bin/python3
#-*- coding: utf-8 -*-

import multiprocessing as mp
import matplotlib.pyplot as plt
import numpy as np
import pickle
import sys

eps = 1.0
a = 1; b = 1
dx = 0.01
dy = 0.01
e0 = 8.85e-12 # 真空中の誘電率
r1 = 2*(dx**2 + dy**2)
r2 = (dx*dy)**2/(e0*r1)
x = np.arange(0, a, dx)
y = np.arange(0, b, dy)
rho = np.zeros([len(x), len(y)])
u = np.zeros([len(x), len(y)])
u_prev = 0

def wrap_jacobi(args):
    return args[0](*args[1:])

def jacobi(j, k):
    global u_prev
    global u
    global eps

    u_prev = u[j][k]
    u[j][k] = rho[j][k]*r2 + (dy**2*(u[j+1][k]+u[j-1][k]) + dx**2*(u[j][k+1]+u[j][k-1]))/r1

    err = abs(u[j][k] - u_prev)
    if eps < err:
        eps = err

# 2次元ポアソン方程式
def main():
    EPS = 1.0e-05

    func_args = []
    for j in range(len(x)): # x
        for k in range(len(y)): # y
            if ((len(x)/2-j)**2 + (len(y)/2-k)**2)*dx*dy < 0.05**2:
                rho[j][k] = 1.0e-08

    for j in range(1, len(x)-1): # x
        for k in range(1, len(y)-1): # y
            func_args.append((jacobi, j, k))

    cnt = 0
    u_max = 1.0e-10
    global eps
    while EPS < eps:
        if cnt % 100 == 0:
            print('cnt = {}, eps = {}, u_max = {}'.format(cnt, eps, u_max))
        eps = 0.0

        with mp.Pool(processes = 8) as pool:
            pool.map(wrap_jacobi, func_args)

        if u_max < np.max(abs(u)):
            u_max = np.max(u)
        eps /= u_max # 現在の最大電位で規格化
        cnt += 1

    print('cnt = {}, eps = {}'.format(cnt, eps))

    plt.imshow(u, cmap='coolwarm')
    pp = plt.colorbar(orientation='vertical')
    pp.set_label('u [V]')
    plt.show()

if __name__=='__main__':
    main()

