#!/usr/bin/python3
#-*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
import pickle
import sys


# 2次元ポアソン方程式
def main():
    a = 10; b = 10
    dx = 0.1
    dy = 0.1
    h = 0.1
    x = np.arange(0, a, dx)
    y = np.arange(0, b, dy)
    u = np.zeros([len(x), len(y)])
    u_prev = np.zeros_like(u)
    rho = np.zeros([len(x), len(y)]) # 電荷密度
    e0 = 8.85e-12 # 真空中の誘電率
    EPS = 1.0e-06
    eps = 1.0e+05

    for j in range(int(len(x)/4), int(len(x)/2)): # x
        for k in range(int(len(y)/4), int(len(y)/2)): # y
            rho[j][k] = 1.0e-08

    r1 = 2*(dx**2 + dy**2)
    r2 = (dx*dy)**2/(e0*r1)
    print('dx**2 = %lf' % (dx**2)) # 安定性解析
    print('dy**2 = %lf' % (dy**2)) # 安定性解析
    print('r1 = %lf' % (r1)) # 安定性解析
    print('r2 = %lf' % (r2)) # 安定性解析
    cnt = 0
    while EPS < eps and cnt < 1.0e+03:
        #u_prev = u
        for j in range(1, len(x)-1): # x
            for k in range(1, len(y)-1): # y
                u_prev = u[j][k]
                #u[j][k] = 0.25*(rho[j][k]/e0*h**2 + u[j+1][k] + u[j-1][k] + u[j][k+1] + u[j][k-1])
                u[j][k] = rho[j][k]*r2 + (dy**2*(u[j+1][k]+u[j-1][k]) + dx**2*(u[j][k+1]+u[j][k-1]))/r1

                #u[j][k] = (u_prev[j+1][k] + u_prev[j-1][k] + u_prev[j][k+1] + u_prev[j][k-1])/4 # worked(Laplace)

        eps = np.max(abs(u - u_prev))
        cnt += 1

    print('cnt = {}'.format(cnt))

    plt.imshow(u, cmap='coolwarm')
    pp = plt.colorbar(orientation='vertical')
    pp.set_label('u')
    plt.show()

if __name__=='__main__':
    main()

