#!/usr/bin/python3
#-*- coding: utf-8 -*-
#
# Initial conditionとBoundary conditionの定義

import matplotlib.pyplot as plt
import numpy as np


# Constant
e0 = 8.85e-12 # 真空中の誘電率
W = 200
H = 200

# 初期条件(Initial condition)
b0 = np.zeros([W+1, H+1])
b0[-2] = 1.0e-08 # 上空
b0 = b0/e0

# 境界条件(Boundary condition)
bc = np.ones_like(b0) # 0: 境界(物体), 1: 計算領域
bc[0:H//2,W//2-1:W//2+1] = 0 # 避雷針
bc[0:2] = 0 # 地面

'''
for j in range(W): # x
    for k in range(H): # y
        if ((W/2-j)**2 + (H/2-k)**2)*h**2 < 0.05**2:
            rho[j][k] = 1.0e-08
'''

np.savetxt("input_IC.txt", b0)
np.savetxt("input_BC.txt", bc)

plt.imshow(b0)
pp = plt.colorbar(orientation='vertical')
plt.xlim([0, W])
plt.ylim([0, H])
plt.show()

plt.imshow(bc, cmap='binary_r')
pp = plt.colorbar(orientation='vertical')
plt.xlim([0, W])
plt.ylim([0, H])
plt.show()
