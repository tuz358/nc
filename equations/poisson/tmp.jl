#!/usr/local/bin/julia

include("./PoissonSolver.jl")
using .PoissonSolver
using CSV
using DataFrames

result = PoissonSolver.solve("input_rho.txt")
CSV.write("result.txt", DataFrame(result), delim=' ',header=false)
