#!/usr/bin/python3
#-*- coding: utf-8 -*-

from scipy.sparse import diags, lil_matrix
import matplotlib.pyplot as plt
import numpy as np
import sys


# 2次元ポアソン方程式(行列化Jacobi, scipyによる高速化)
def main():
    # Constant
    e0 = 8.85e-12 # 真空中の誘電率
    EPS = 1.0e-05
    eps = 1.0
    a = 1
    h = 0.01 # 空間刻み幅
    N = int(a/h)-2

    phi_new = np.zeros([N, N]).flatten()
    phi = np.zeros_like(phi_new)
    rho = np.zeros([N, N])

    print('h**2 = %lf' % (h**2))
    # 電荷を配置
    for j in range(N): # x
        for k in range(N): # y
            if ((N/2-j)**2 + (N/2-k)**2)*h**2 < 0.05**2:
                rho[j][k] = 1.0e-08

    I = np.eye(N)
    J = diags([1, 1], [-1, 1], shape=(N, N)).toarray()

    A = -4*np.kron(I, I) + np.kron(J, I) + np.kron(I, J)
    D = np.diagflat(np.diag(A))
    D_inv = np.reciprocal(D) # D_inv = np.linalg.inv(D)と等価(Dは対角行列のため)
    D_inv[D_inv == np.inf] = 0 # 上のreciprocalで0除算が発生するため
    L = np.tril(A) - D
    U = np.triu(A) - D
    R = L + U
    b = -h**2/e0 * rho.flatten() # b = -rho.flatten()/e0

    A.astype(np.float32)
    D_inv.astype(np.float32)
    L.astype(np.float32)
    U.astype(np.float32)
    b.astype(np.float32)

    A = lil_matrix(A).tocsr()
    L = lil_matrix(L).tocsr()
    D_inv = lil_matrix(D_inv).tocsr()
    U = lil_matrix(U).tocsr()
    R = lil_matrix(R).tocsr()
    b = lil_matrix(b).T.tocsr()
    phi_new = lil_matrix(phi_new).T.tocsr()
    phi = lil_matrix(phi).T.tocsr()

    cnt = 0
    while EPS < eps:
        if cnt % 1000 == 0:
            print('cnt = {}, eps = {}'.format(cnt, eps))

        phi_new = D_inv.dot(b - R.dot(phi))

        eps = np.linalg.norm(phi_new.toarray() - phi.toarray(), np.inf)
        phi = phi_new
        cnt += 1

    print('cnt = {}, eps = {}'.format(cnt, eps))

    phi_new = phi_new.toarray()

    plt.imshow(phi_new.reshape(N, N), cmap='coolwarm')
    pp = plt.colorbar(orientation='vertical')
    pp.set_label('phi [V]')
    plt.show()

if __name__=='__main__':
    main()

