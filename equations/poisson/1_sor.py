#!/usr/bin/python3
#-*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
import pickle
import sys


# 2次元ポアソン方程式(SOR method)
def main():
    a = 1; b = 1
    dx = 0.01
    dy = 0.01
    x = np.arange(0, a, dx)
    y = np.arange(0, b, dy)
    u = np.zeros([len(x), len(y)])
    rho = np.zeros([len(x), len(y)])
    e0 = 8.85e-12 # 真空中の誘電率
    EPS = 1.0e-05
    eps = 1.0
    omega = 1.8 # 過緩和パラメータ

    for j in range(len(x)): # x
        for k in range(len(y)): # y
            if ((len(x)/2-j)**2 + (len(y)/2-k)**2)*dx*dy < 0.05**2:
                rho[j][k] = 1.0e-08

    # 境界条件
    #u[0][:], u[-1][:], u[:][0], u[:][-1] = 5, 0, 0, 0

    u_prev = 0
    r1 = 2*(dx**2 + dy**2)
    r2 = (dx*dy)**2/(e0*r1)
    print('dx**2 = %lf' % (dx**2))
    print('dy**2 = %lf' % (dy**2))
    print('r1 = %lf' % (r1))
    print('r2 = %lf' % (r2))
    u_max = 1.0e-10
    cnt = 0
    while EPS < eps:
        if cnt % 100 == 0:
            print('cnt = {}, eps = {}, u_max = {}'.format(cnt, eps, u_max))
        eps = 0.0
        for j in range(1, len(x)-1): # x
            for k in range(1, len(y)-1): # y
                u_prev = u[j][k]
                u[j][k] = u[j][k] + omega*(rho[j][k]*r2 + (dy**2*(u[j+1][k]+u[j-1][k]) + dx**2*(u[j][k+1]+u[j][k-1]))/r1 - u[j][k])
                # dx = dy = hのとき
                #u[j][k] = 0.25*(rho[j][k]*h*h/e0+u[j+1][k]+u[j-1][k]+u[j][k+1]+u[j][k-1])

                err = abs(u[j][k] - u_prev)
                if eps < err:
                    eps = err

        if u_max < np.max(abs(u)):
            u_max = np.max(u)
        eps /= u_max # 現在の最大電位で規格化
        cnt += 1

    print('cnt = {}, eps = {}'.format(cnt, eps))

    plt.imshow(u, cmap='coolwarm')
    pp = plt.colorbar(orientation='vertical')
    pp.set_label('u [V]')
    plt.show()

if __name__=='__main__':
    main()

