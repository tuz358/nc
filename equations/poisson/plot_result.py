#!/usr/bin/python3
#-*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt


box_linewidth = 1.6
fig = plt.figure()
#ax = fig.add_subplot(111)
#ax.set_aspect('equal', adjustable='box')
plt.rcParams["xtick.direction"] = "out"
plt.rcParams["ytick.direction"] = "out"
plt.rcParams["xtick.minor.visible"] = False
plt.rcParams["ytick.minor.visible"] = False
plt.rcParams["xtick.top"] = False
plt.rcParams["ytick.right"] = False
plt.rcParams["xtick.major.width"] = box_linewidth
plt.rcParams["ytick.major.width"] = box_linewidth
plt.rcParams["xtick.minor.width"] = box_linewidth
plt.rcParams["ytick.minor.width"] = box_linewidth
plt.rcParams["xtick.major.size"] = 5
plt.rcParams["ytick.major.size"] = 5
plt.rcParams["xtick.minor.size"] = 3
plt.rcParams["ytick.minor.size"] = 3
plt.rcParams["font.size"] = 16
plt.rcParams["axes.linewidth"] = box_linewidth
plt.rcParams["legend.fancybox"] = False
plt.rcParams["legend.fontsize"] = 9
plt.rcParams["legend.frameon"] = False
plt.rcParams["legend.loc"] = 'best'



result = np.loadtxt('result.txt')
plt.plot([100, 100], [0, 100], color='r', linewidth=2, alpha=0.7)
plt.text(x=105, y=90, s='Lightning rod', size=10, c='r', fontweight='bold')
plt.grid(color='k', linewidth=1.6, linestyle='--', alpha=0.2)
plt.contour(result, levels=50, alpha=0.8, linewidths=0.5)
plt.imshow(result, alpha=0.8)#plt.imshow(result, cmap='inferno')
pp = plt.colorbar(orientation='vertical')

xlim = result.shape[0]-1
ylim = result.shape[1]-1
plt.xlim([0, xlim])
plt.ylim([0, ylim])
plt.xticks(np.linspace(0, xlim, 5))
plt.yticks(np.linspace(0, ylim, 5))
plt.xlabel('x (a.u.)')
plt.ylabel('y (a.u.)')
pp.set_label('Potential, $\phi$ (V)')

fig.tight_layout()
plt.show()
