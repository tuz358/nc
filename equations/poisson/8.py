#!/usr/bin/python3
#-*- coding: utf-8 -*-

from scipy.sparse import diags, lil_matrix, linalg
import numpy as np
import sys


# Constant
e0 = 8.85e-12 # 真空中の誘電率
EPS = 1.0e-05
a = 1
dx = 0.01
dy = 0.01
W = int(a/dx)-2
H = int(a/dy)-2
omega = 1.8


# 2次元Poisson方程式(行列化SOR, scipyによる高速化)
class PoissonSolver2D(object):
    def __init__(self, rho):
        print('dx**2 = %lf' % (dx**2))
        self.rho = rho

    def solve(self):
        phi_new = np.zeros([W, H]).flatten()
        phi = np.zeros_like(phi_new)

        Im = np.eye(W)
        In = np.eye(H)
        Jm = diags([1, 1], [-1, 1], shape=(W, H)).toarray()
        Jn = diags([1, 1], [-1, 1], shape=(W, H)).toarray()

        A = np.kron(In, (2*Im - Jm)/dx**2) + np.kron((2*In - Jn)/dy**2, Im)
        D = np.diagflat(np.diag(A))
        D_inv = np.reciprocal(D, where=D!=0) # D_inv = np.linalg.inv(D)と等価(Dは対角行列のため)
        #D_inv[D_inv == np.inf] = 0 # 上のreciprocalで0除算が発生するため
        L = np.tril(A) - D
        U = np.triu(A) - D
        b = -rho.flatten()/e0 # b = -rho.flatten()/e0

        A.astype(np.float32)
        D.astype(np.float32)
        L.astype(np.float32)
        U.astype(np.float32)
        b.astype(np.float32)

        A = lil_matrix(A).tocsc()
        D = lil_matrix(D).tocsc()
        L = lil_matrix(L).tocsc()
        U = lil_matrix(U).tocsc()
        b = lil_matrix(b).T.tocsc()

        tmp = linalg.inv(D + omega*L) # tmp = spsolve(D + omega*L, np.eye(N**2))
        M = np.dot(tmp, (1-omega)*D - omega*U)
        N = omega*tmp

        M.astype(np.float32)
        N.astype(np.float32)

        M = lil_matrix(M).tocsc()
        N = lil_matrix(N).tocsc()
        phi_new = lil_matrix(phi_new).T.tocsc()
        phi = lil_matrix(phi).T.tocsc()

        cnt = 0; eps = 1.0
        while EPS < eps:
            if cnt % 100 == 0:
                print(f'cnt = {cnt}, eps = {eps:e}')

            phi_new = M.dot(phi) + N.dot(b)

            eps = np.linalg.norm(phi_new.toarray() - phi.toarray(), np.inf)
            phi = phi_new
            cnt += 1

        print(f'cnt = {cnt}, eps = {eps:e}')
        phi_new = phi_new.toarray()
        return phi_new.reshape(W, H)


if __name__=='__main__':
    rho = np.loadtxt('input_rho.txt')

    solver = PoissonSolver2D(rho)
    result = solver.solve()

    np.savetxt('result.txt', result)
