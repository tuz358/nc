#!/usr/bin/python3
#-*- coding: utf-8 -*-

from matplotlib.animation import ArtistAnimation
import matplotlib.pyplot as plt
import numpy as np
import pickle
import sys

fig = plt.figure()
anim = []


# 2次元ポアソン方程式
def main():
    a = 10; b = 10
    dx = 0.1
    dy = 0.1
    h = 0.1
    x = np.arange(0, a, dx)
    y = np.arange(0, b, dy)
    u = np.zeros([len(x), len(y)])
    u_prev = np.zeros_like(u)
    rho = np.zeros([len(x), len(y)]) # 電荷密度
    e0 = 8.85e-12 # 真空中の誘電率
    EPS = 1.0e-06
    eps = 1.0e+05

    metal = np.zeros([len(x), len(y)])


    for j in range(int(len(x)/4), int(len(x)*3/4)): # x
        for k in range(int(len(y)/4), int(len(y)*3/4)): # y
            rho[j][k] = 1.0e-08

    for j in range(int(len(x)/2), int(len(x)*3/4)): # x
        for k in range(int(len(y)/2), int(len(y)*3/4)): # y
            metal[j][k] = 1

    im = plt.imshow(u, cmap='coolwarm')
    anim.append([im])

    r1 = 2*(dx**2 + dy**2)
    r2 = (dx*dy)**2/(e0*r1)
    print('dx**2 = %lf' % (dx**2)) # 安定性解析
    print('dy**2 = %lf' % (dy**2)) # 安定性解析
    print('r1 = %lf' % (r1)) # 安定性解析
    print('r2 = %lf' % (r2)) # 安定性解析
    cnt = 0
    while EPS < eps and cnt < 1.0e+03:
        for j in range(1, len(x)-1): # x
            for k in range(1, len(y)-1): # y
                if metal[j][k] == 0:
                    u_prev = u[j][k]
                    u[j][k] = rho[j][k]*r2 + (dy**2*(u[j+1][k]+u[j-1][k]) + dx**2*(u[j][k+1]+u[j][k-1]))/r1

        eps = np.max(abs(u - u_prev))
        cnt += 1

        im = plt.imshow(u, cmap='coolwarm')
        anim.append([im])

    print('cnt = {}'.format(cnt))

    ani = ArtistAnimation(fig, anim, interval=1, blit=True)

    #plt.imshow(u, cmap='coolwarm')
    #pp = plt.colorbar(orientation='vertical')
    #pp.set_label('u')
    plt.show()

if __name__=='__main__':
    main()

