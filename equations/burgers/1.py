#!/usr/bin/python3
#-*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import sys

def gauss(x, a=1, mu=0, sigma=1):
    return a*np.exp(-(x - mu)**2 / (2*sigma**2))

# Burgers equation
# 時間項 : オイラー陽解法
# 移流項 : 1次精度中心差分
# 拡散項 : 2次精度中心差分
def main():
    a = 10; b = 5
    dx = 0.1
    dt = 0.001
    nu = 0.01 # 粘性係数
    x = np.arange(0, a, dx)
    t = np.arange(0, b, dt)
    u = np.zeros([len(t), len(x)])

    r = dt/dx

    # 初期条件
    u[0] = 10*gauss(x, mu=3.0) # t = 0
    for j in range(1, len(x)-1): # t = 1
        u[1][j] = u[0][j] - u[0][j]*0.5*r*(u[0][j+1] - u[0][j-1])
    # ノイマン境界条件
    u[:][0] = u[:][1]; u[:][-1] = u[:][-2]

    print('r = %lf' % (r)) # 安定性解析
    print('nu*r/dx = %lf' % (nu*r/dx)) # 安定性解析

    for i in range(1, len(t)-1): # t
        for j in range(1, len(x)-1): # x
            u[i+1][j] = u[i][j] - 0.5*r*u[i][j]*(u[i][j+1] - u[i][j-1]) + nu*r/dx*(u[i][j+1] - 2*u[i][j] + u[i][j-1])

    fig = plt.figure()
    frame = len(t)
    #frame = int(len(t)*0.1)
    def update(i):
        if i != 0:
            fig.clear()
        #plt.ylim(-2, 10)
        plt.plot(x, u[i], color='blue')
        sys.stdout.write('\rexporting {:.3f} %'.format(100*i/frame))
        sys.stdout.flush()


    ani = animation.FuncAnimation(fig, update, interval=1, frames=frame)
    fig.show()
    ani.save('output.gif', writer='imagemagick')

if __name__=='__main__':
    main()

