# nc

数値計算の練習用リポジトリ

| Algorithm | Path |
|:---:|:---:|
| Euler method | euler/ |
| Euler central diff method | central_diff/ |
| Euler backward diff method | backward_diff/ |
| Runge-Kutta method | rk4/ |
| Heun method | heun/ |
| Runge-Kutta-Fehlberg method | fehlberg/ |
| FTCS method | ftcs/ |
| Lax-Friedrichs method | lax-f/ |
| Lax-Wendroff method | lax-w/ |
| Jacobi method | jacobi/ |
| Gauss-Seidel method | gauss-seidel/ |
| SOR method | sor/ |

| Equation | Path |
|:---:|:---:|
| Burgers equation | burgers/ |
| Poisson's equation | poisson/ |
| Navier-Stokes equations | navier-stokes/ |

## Requirements
- Python3
- numpy
- matplotlib
- scipy

